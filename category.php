<?php
$date=new DateTime();
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- <link rel="stylesheet" href="assets/css/decor.css"> -->

    <title>Pokemon MF</title>
</head>
<body>

    <!-- head -->
    <div class="container-fluid">
        <div class="row">

            <div class="nav1 fixed-top">
                <nav class="navbar navbar-light">
                    <a class="navbar-brand" href="#" id="header"><b>ADMIN PANEL</b></a>
                    <a id="logout" href="registrasi/logout.php"><button  class="btn btn-primary text-warning border-warning bg-transparent"><b>Log Out</b></button></a>
                </nav>
            </div>

            <div class="nav2 fixed-top">
                <nav class="navbar navbar-dark">
                    <button class="navbar-toggler" disabled data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <span class="text-white" style="margin-left: 70%;"><?php echo $date->format('d F Y'); ?></span>
                    <a href="registrasi/logout.php" class="btn text-warning border-warning bg-transparent"><b>Log Out</b></a>
                </nav>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row">

            <div class="body position-fixed" style="color: rgb(190, 190, 190);">
                <p class="text-white p-1 pt-3">
                    <svg width="2.5em" height="2.5em" viewBox="0 0 16 16" class="bi bi-person-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13.468 12.37C12.758 11.226 11.195 10 8 10s-4.757 1.225-5.468 2.37A6.987 6.987 0 0 0 8 15a6.987 6.987 0 0 0 5.468-2.63z"/>
                        <path fill-rule="evenodd" d="M8 9a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        <path fill-rule="evenodd" d="M8 1a7 7 0 1 0 0 14A7 7 0 0 0 8 1zM0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8z"/>
                    </svg>
                    <span class="pl-2" style="font-size: 1.1em;">Muhammad Ferdiansyah</span>
                </p>
                <p class="p-3" style="background-color: black;">Main course</p>
                <a href="index.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-house" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                        </svg>
                        <span class="pl-3">Home</span>
                    </p>
                </a>
                <a href="dashboard.php" id="satu" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-box" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5 8 5.961 14.154 3.5 8.186 1.113zM15 4.239l-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z"/>
                        </svg>
                        <span class="pl-3">Dashboard</span>
                    </p>
                </a>
                <a href="newPokemon.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-bug-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.978.855a.5.5 0 1 0-.956.29l.41 1.352A4.985 4.985 0 0 0 3 6h10a4.985 4.985 0 0 0-1.432-3.503l.41-1.352a.5.5 0 1 0-.956-.29l-.291.956A4.978 4.978 0 0 0 8 1a4.979 4.979 0 0 0-2.731.811l-.29-.956zM13 6v1H8.5v8.975A5 5 0 0 0 13 11h.5a.5.5 0 0 1 .5.5v.5a.5.5 0 1 0 1 0v-.5a1.5 1.5 0 0 0-1.5-1.5H13V9h1.5a.5.5 0 0 0 0-1H13V7h.5A1.5 1.5 0 0 0 15 5.5V5a.5.5 0 0 0-1 0v.5a.5.5 0 0 1-.5.5H13zm-5.5 9.975V7H3V6h-.5a.5.5 0 0 1-.5-.5V5a.5.5 0 0 0-1 0v.5A1.5 1.5 0 0 0 2.5 7H3v1H1.5a.5.5 0 0 0 0 1H3v1h-.5A1.5 1.5 0 0 0 1 11.5v.5a.5.5 0 1 0 1 0v-.5a.5.5 0 0 1 .5-.5H3a5 5 0 0 0 4.5 4.975z"/>
                        </svg>
                        <span class="pl-3">New Pokemon</span>
                    </p>
                </a>
                <a href="bestPokemon.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-hand-thumbs-up" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M6.956 1.745C7.021.81 7.908.087 8.864.325l.261.066c.463.116.874.456 1.012.965.22.816.533 2.511.062 4.51a9.84 9.84 0 0 1 .443-.051c.713-.065 1.669-.072 2.516.21.518.173.994.681 1.2 1.273.184.532.16 1.162-.234 1.733.058.119.103.242.138.363.077.27.113.567.113.856 0 .289-.036.586-.113.856-.039.135-.09.273-.16.404.169.387.107.819-.003 1.148a3.163 3.163 0 0 1-.488.901c.054.152.076.312.076.465 0 .305-.089.625-.253.912C13.1 15.522 12.437 16 11.5 16v-1c.563 0 .901-.272 1.066-.56a.865.865 0 0 0 .121-.416c0-.12-.035-.165-.04-.17l-.354-.354.353-.354c.202-.201.407-.511.505-.804.104-.312.043-.441-.005-.488l-.353-.354.353-.354c.043-.042.105-.14.154-.315.048-.167.075-.37.075-.581 0-.211-.027-.414-.075-.581-.05-.174-.111-.273-.154-.315L12.793 9l.353-.354c.353-.352.373-.713.267-1.02-.122-.35-.396-.593-.571-.652-.653-.217-1.447-.224-2.11-.164a8.907 8.907 0 0 0-1.094.171l-.014.003-.003.001a.5.5 0 0 1-.595-.643 8.34 8.34 0 0 0 .145-4.726c-.03-.111-.128-.215-.288-.255l-.262-.065c-.306-.077-.642.156-.667.518-.075 1.082-.239 2.15-.482 2.85-.174.502-.603 1.268-1.238 1.977-.637.712-1.519 1.41-2.614 1.708-.394.108-.62.396-.62.65v4.002c0 .26.22.515.553.55 1.293.137 1.936.53 2.491.868l.04.025c.27.164.495.296.776.393.277.095.63.163 1.14.163h3.5v1H8c-.605 0-1.07-.081-1.466-.218a4.82 4.82 0 0 1-.97-.484l-.048-.03c-.504-.307-.999-.609-2.068-.722C2.682 14.464 2 13.846 2 13V9c0-.85.685-1.432 1.357-1.615.849-.232 1.574-.787 2.132-1.41.56-.627.914-1.28 1.039-1.639.199-.575.356-1.539.428-2.59z"/>
                        </svg>
                        <span class="pl-3">Best Pokemon</span>
                    </p>
                </a>
                <a href="category.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-book" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                        </svg>
                        <span class="pl-3">Article</span>
                    </p>
                </a>
                <a href="produk.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-cart3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                        </svg>
                        <span class="pl-3">Produk</span>
                    </p>
                </a>
                <a href="about.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.3em" height="1.3em" viewBox="0 0 16 16" class="bi bi-file-earmark-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4 0h5.5v1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h1V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2z"/>
                            <path d="M9.5 3V0L14 4.5h-3A1.5 1.5 0 0 1 9.5 3z"/>
                            <path fill-rule="evenodd" d="M8 11a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                            <path d="M8 12c4 0 5 1.755 5 1.755V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-.245S4 12 8 12z"/>
                        </svg>
                        <span class="pl-3">About</span>
                    </p>
                </a>
                <a href="contact.php" style="text-decoration: none;">
                    <p class="pl-3 p-2">
                        <svg width="1.3em" height="1.3em" viewBox="0 0 16 16" class="bi bi-telephone-inbound-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 
                            2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 
                            1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM15.854.146a.5.5 0 0 1 0 
                            .708L11.707 5H14.5a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5v-4a.5.5 0 0 1 1 0v2.793L15.146.146a.5.5 0 0 1 .708 0z"/>
                        </svg>
                        <span class="pl-3">Contact</span>
                    </p>
                </a>
            </div>

            <div class="body2 p-3 bg-dark">
                <div class="article">
                
                <p class="text-center text-danger" style="font-size: 3em;">Article</p>
                <p class="text-white border-bottom" style="font-size: 2em;">Pokemon</p>
                <p class="text-white">from indonesian Wikipedia, free encyclopedia</p>
                <br>
                <p class="text-white">
                Pokémon (ポ ケ モ ン Pokemon, pronunciation: / ˈpoʊkeɪmɒn / [2] [3]) is a media franchise owned by the video game company Nintendo and created by Satoshi 
                Tajiri in 1995. Initially, Pokémon was a video game series synonymous with game consoles. Boy. Pokémon is the second most successful video game in the world 
                after the Mario series which was also created by Nintendo. [4] Meanwhile, when compared to others, Pokémon is the third best-selling franchise series in the 
                world after James Bond (by Ian Fleming) and Transformers (by Hasbro), which is still running and being updated according to the times. The Pokémon franchise 
                itself appears in various forms, namely video games, anime, manga, trading cards, books, toys, and many more. The media franchise celebrated its tenth 
                anniversary on February 27, 2006. And as of April 23, 2008, Pokémon video game sales have reached 180 million copies [5], surpassing the sales of Transformers 
                video games.The name Pokémon is a name romanized from the Japanese name "Pocket Monsters" (ポ ケ ッ ト モ ン ス タ ー Poketto Monsutā). [6] The word "Pokémon", 
                apart from referring to the Pokémon franchise itself, also refers to the 809 fictional species that appear in all of the Pokémon series. Like the words deer 
                and sheep, the singular and plural Pokémon in English are no different. The word Pokémon in English can refer to just one Pokémon or more than one.In November 
                2005, 4Kids Entertainment, which is a Pokémon distributor outside Japan, announced that they would not renew their contract with The Pokémon Company Japan. 
                Realizing that The Pokémon Company Japan would not be successful without the help of partners from outside Japan, The Pokémon Company Japan then created a 
                subsidiary based in America. The company became known as The Pokémon Company International. His job is to become the distributor and license holder for Pokémon 
                for regions outside Asia, including Africa. [7]In July 2016, a smartphone augmented reality game called Pokémon Go went public. [8] The next generation of video 
                games, Pokémon Sun and Moon, will launch in November 2016. [9]
                </p>
                <br>
                <p class="text-white border-bottom" style="font-size: 2em;">Basic concepts</p>
                <p class="text-white">
                The original concept of Pokémon, as it appears in video games and all Pokémon fiction in general, was the adoption of the hobby of collecting insects, which 
                Satoshi Tajiri had done when he was a child. [10] In video games, the player is referred to as a Pokémon Trainer (en: Pokémon trainer). There are three general 
                goals that every Pokémon trainer should accomplish in order to supplement the contents of the Pokédex. First, you have to capture all the Pokémon species in the 
                region you are in. Second, you are required to develop the Pokémon species that you have to become strong and then be able to win against other challengers, and 
                the additional objective is to defeat Team Rocket. Third or last, if the Pokémon you are educating is strong enough physically and technically, you have a big 
                chance to fight against the strongest trainer, which is then called the Pokémon Master. All of these are in the entire Pokémon franchise, including video games, 
                anime, manga, and trading cards.In the majority of the Pokémon series, a trainer who can block the power of a wild Pokémon can catch it and make it its own pet 
                by throwing an object that can shrink the Pokémon's density, which is then called the Poké Ball. With the capture of the wild Pokémon, the trainer officially 
                owns the wild Pokémon. Pokémon that are owned by other people cannot be forcibly taken over with Poké Balls unless there are other things that the two fighting 
                trainers agree on, such as trading, or buying them. The Pokémon that we have in our future can be fought with wild Pokémon or with other trainers. For the Pokémon 
                who wins the battle, of course its strength will increase and increase. They gain experience points, and have the opportunity to go to higher levels when the time 
                comes. When they level up, their stats will automatically increase, such as attack, defense, speed, and many more. From time to time, depending on the level, the 
                Pokémon then become acquainted with several new attack tactics, which can be used in combat. On several occasions, there are most types of Pokémon which when they 
                reach a certain level or by fulfilling other requirements such as using evolution stones and certain items (such as rare candy), will metamorphose into a new species, 
                which is then known as evolution.In the video game series, we have to act as a trainer who builds and raises a Pokémon team to defeat the various types of Pokémon 
                that our rival or rival trainers have, as well as of course to defeat various wild Pokémon and other trainers. The layout of the Pokémon game is mostly linear, and 
                surrounds a fictional region in the Pokémon world (because the planet Earth version of the Pokémon world is different from the fictional version of Earth used in 
                series such as Transformers or James Bond, which depicts the world in its true state as we live in today. ). In each Pokémon game, there are eight Pokémon specialist 
                masters who are then referred to as Gym Leaders (id: jim leader). We as coaches have to beat the gym leaders one by one if we want to get a badge / pin which we can 
                then use as a sign of entry to the Pokémon League national championship (Pokémon League). Where there we will challenge four Pokémon elders who are then called The 
                Elite Four. And if we manage to win against the Elite Four, we will be taken to challenge the Regional Champion, and if we manage to win against the Regional Champion, 
                then we will be declared National Champion and entitled to the name Pokémon Master (can also be International under the name Pokémon Super Master ") .
                </p>
                </div>

                    
            </div>

        </div>
    </div>
    
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   
</body>
</html>