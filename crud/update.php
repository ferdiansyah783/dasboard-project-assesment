<?php

require 'functions.php';

// ambil data di url
$id= $_GET['id'];

// query data berdasarkan id
$pkmn= comand("SELECT * FROM pokemon WHERE id=$id")[0];

// cek tombol submit
if (isset($_POST['submit'])){

    // cek apakah data berhasil diubah
    if (update($_POST) > 0){
        echo "<script>
                alert('data was updated successfully');
                document.location.href='../newPokemon.php';
            </script>
        ";
    }else{
        echo "<script>
                alert('data failed to update');
                document.location.href='../newPokemon.php';
            </script>
        ";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Update</title>
    <style>
        body{
            background-image: url(../assets/images/background3.jpg);
            background-size: cover;
        }
    </style>
</head>
<body>
    
    <!-- update -->
    <div class="container mt-5">
        <h2 class="text-primary text-center border border-primary rounded p-2">Upadate Pokemon</h2>
        <div class="row">
            <div class="col">

                <form action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= $pkmn['id']; ?>">
                    <input type="hidden" name="gambar" value="<?= $pkmn['gambar']; ?>">
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="text-light">Name</label>
                        <input value="<?= $pkmn['nama']; ?>" type="text" name="nama" maxlength="15" autocomplete="off" placeholder="input nama" class="form-control bg-transparent text-info border-info" required autofocus>
                    </div>
                    <div class="form-group">
                        <label class="text-light">Type</label>
                        <select class="custom-select text-info bg-transparent border-info" name="elemen" id="validationCustom04" required>
                            <option selected disabled>Choose type</option>
                            <option value="Fire">Fire</option>
                            <option value="Water">Water</option>
                            <option value="Soil">Soil</option>
                            <option value="Electricity">Electricity</option>
                            <option value="Leaf">Leaf</option>
                            <option value="Fairy">Fairy</option>
                            <option value="Drakness">Drakness</option>
                            <option value="Dragon">Dragon</option>
                            <option value="Ice">Ice</option>
                            <option value="Flying">Flying</option>
                            <option value="Metal">Metal</option>
                            <option value="Fighter">Fighter</option>
                            <option value="Ghost">Ghost</option>
                            <option value="Insect">Insect</option>
                            <option value="Coral">Coral</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="text-light">Additional Elements</label>
                        <input type="text" value="<?= $pkmn['skill']; ?>" maxlength="10" name="skill" autocomplete="off" placeholder="input additional elements" class="form-control bg-transparent text-info border-info" required>
                    </div>
                    <div class="form-group">
                        <label for="elemen" class="text-light">Choose Image</label>
                        <br>
                        <img src="../assets/images/<?= $pkmn['gambar']; ?>" width="55" alt="">
                        <input type="file" name="gambar" autocomplete="off" placeholder="Choose Image" class="bg-transparent text-info border-info" required>
                    </div>

                    <button type="submit" name="submit" class="btn bg-transparent btn-outline-info text-info">Update</button>
                </form>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>

