<?php
session_start();

if (isset($_SESSION['login'])){
    header("location: ../index.php");
    exit;
}

require 'register.php';

if (isset($_POST['login'])){
    $username= $_POST['username'];
    $password= $_POST['password'];

    $result= mysqli_query($conn, "SELECT * FROM user WHERE username= '$username'");

    // cek username
    if (mysqli_num_rows($result)===1){

        // cek password
        $row= mysqli_fetch_assoc($result);
        if(password_verify($password, $row["password"])){
            // set session
            $_SESSION['login']= true;

            header("location: ../index.php");
            exit;
        }
    }
    $error= true;
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Sign In</title>
    <style>
        body{
            background-image: url(../assets/images/background4.jpg);
            background-size: cover; 
        }
        .foot{
            margin-left: 12em;
        }

        @media only screen and (max-width: 767px){
            .foot{
                margin-left: 1em;
                font-size: small;
            }
        }
    </style>
</head>
<body>
    
        <div class="container">
            <div class="row justify-content-center" style="margin-top: 10%;">
                <div class="col-10 col-sm-5 col-md-5 col-lg-5">

                    <h1 class="text-center text-info col-sm-6 col-md-6 col-lg-6">ADMIN<span style="color: red;">.LOG</span></h1>
                    <?php if (isset($error)) : ?>
                        <h5 style="color: red;">username or password is false</h5>
                    <?php endif; ?>

                    <div class="border p-3 bg-trnsparent border-primary" style="border-radius: 0.5em;">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="text-light">Username</label>
                                <input type="text" name="username" class="form-control border-info text-info bg-transparent" placeholder="username" required autocomplete="off" autofocus id="exampleInputEmail1" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="text-light">Password</label>
                                <input type="password" name="password" class="form-control border-info text-info bg-transparent" placeholder="password" required autocomplete="off" autofocus id="exampleInputPassword1">
                            </div>
                            <button type="submit" name="login" class="btn btn-outline-info bg-transparent text-info">Sign In</button>
                            <a style="text-decoration:none;" class="foot" href="registrasi.php">Create new acount</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
       
</body>
</html>