<?php

require 'register.php';

if (isset($_POST['register'])){
    if ( registrasi($_POST) > 0){
        echo "<script>
            alert ('user baru berhasil ditambahkan!');
            </script>";
    }else{
        echo mysqli_error($conn);
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Sign Up</title>
    <style>
        label{
            display: block;
        }
        body{
            background-image: url(../assets/images/bg2.png);
            background-size: cover;
        }
        #sign{
            margin-left: 60%;
        }
        #head{
            font-size: 3em;
        }
        @media only screen and (max-width: 767px){
            #head{
                /* margin-left: 1em; */
                font-size: x-large;
            }
            #sign{
                margin-left: 20%;
            }
        }
    </style>
</head>
<body>
    
    <div class="container">
        <div class="row justify-content-center" style="margin-top: 10%;">
            <div class="col-10 col-sm-10 col-md-5 col-lg-5">
                <p id="head" class="text-center text-info">ADMIN<span style="color: red;">.SignUp</span></p>
                <?php if (isset($error)) : ?>
                    <h5 style="color: red;">confirm password is false</h5>
                <?php endif; ?>

                <div class="border p-3 bg-trnsparent border-primary" style="border-radius: 0.5em;">
                    <form action="" method="POST">
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="text-light">Username</label>
                            <input type="text" name="username" class="form-control border-info text-info bg-transparent" placeholder="input username" required autocomplete="off" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-light">Password</label>
                            <input type="password" name="password" class="form-control border-info text-info bg-transparent" placeholder="input password" required autocomplete="off" id="exampleInputPassword1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-light">Password</label>
                            <input type="password" name="password2" class="form-control border-info text-info bg-transparent" placeholder="confirm password" required autocomplete="off" id="exampleInputPassword1">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" required class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label text-light" for="exampleCheck1">Menyetujui kebijakan dan privasi</label>
                        </div>
                        <button type="submit" name="register" class="btn btn-outline-info bg-transparent text-info">Sign up</button>
                        <a href="login.php" class="btn btn-primary bg-transparent text-info" id="sign">Sign in</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

</body>
</html>